import {FaTimes} from 'react-icons/fa'

function Task({task, onDelete, onReminder}) {
    return (
        <div className={`task ${task.reminder ? 'reminder' : ''}`} onDoubleClick={() => onReminder(task.id)}    
        >
            <h3 key={task.id}>
            {task.text}
            <FaTimes 
                style={{color:'red', cursor: 'pointer'}} 
                onClick={() => onDelete(task.id)}
            />
            </h3>
            <p>
                {task.day}
            </p>
        </div>
    )
}

export default Task
