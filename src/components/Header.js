import React from 'react'
import Button from './Button'

function Header({title, onAddTask,showAddTask}) {

    return (
        <div className='header'>
           <h1>{title}</h1> 
           <Button text={showAddTask? 'Close' : 'Add'} color={showAddTask ? 'red' : 'green'} onClick={onAddTask}/>
        </div>
    )
}

export default Header
