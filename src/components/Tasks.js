import Task from "./Task"

function Tasks({tasks, onDelete, onReminder}) {
    return (
        <>
          {tasks.map((task) => {
              return (<Task task={task} onDelete={onDelete} onReminder={onReminder}/> )
          })}  
        </>
    )
}

export default Tasks
